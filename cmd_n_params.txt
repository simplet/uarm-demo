serial_device_settings = {
            'baudrate': 115200,
            'timeout': 10,
}    


cmd = {
        'set_pump': 'M2231 V{on}',
        'set_wrist': 'G2202 N3 V{angle} F{speed}',
        'set_position': 'G0 X{x} Y{y} Z{z} F{speed}',
        'get_position': 'P2220',
        'get_power_status': 'P2234',
        'get_device_type': 'P2201',
        'get_device_unique': 'P2205',
        'get_device_firmware': 'P2203',
        'get_device_hardware': 'P2202',
        'get_mode': 'P2400',
        'get_servo_attach': 'P2206 N{id}',
        'get_pump': "P2231",
        'get_is_moving': "M2200",
        'set_servo_angle': "G2202 N{id} V{angle} F{speed}",
        'set_buzzer': "M2210 F{f} T{time}"
    }